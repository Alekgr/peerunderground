<?php
		/* join2.php used for processing joining(registering) of a user; */

		require_once($_SERVER['DOCUMENT_ROOT']."/defs.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/email.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/db/db_users.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/db/db_functions.php");

		error_reporting(E_ALL);


		$username	= 	$_POST['username'];
		$first		= 	$_POST['first'];
		$last		=	$_POST['last'];			 	
		$pass1 		= 	$_POST['pass1'];
		$pass2	 	=	$_POST['pass2'];
		$email		=	$_POST['email'];


		$s	=	generateSalt();
		$h	=	generateHash($s,$pass1);
	
		$conn =	db_connect();
		if(db_connect_ok($conn)==false)
		{
				printf("%s", db_connect_msg($conn));
				printf("%d", db_connect_errorcode($conn));
		}
		
		db_insertnewuser($conn,$first,$last,$username,$h,$email);	
		$r=db_close($conn);
		emailjoin($email,$username,$first,$last);	

		function generateSalt()
		{
				/*generate a salt*/
				
				$salt = mcrypt_create_iv(28,MCRYPT_DEV_URANDOM); 
				$salt = base64_encode($salt);	
				$salt = strtr($salt, '+','.'); 
				
				return	$salt;
		}


		function generateHash($salt, $pass1)
		{
				/*generate a hash from a password using unique and random salt using blowfish encryption 2y$ with 12$ cost*/
				
				$hash	=	crypt($pass1, '$2y$12$' . $salt);
				return $hash;
		}


		function validate_firstname($first)
		{
				/* first name must no more than 20 characters
				   can only contain a-z and A-Z  characters	*/

				if(strlen($first)==FIRSTNAME_MAX_SIZE)
				{
						echo "first name size too big<br>";
						return(ERROR_JOIN_FIRSTNAME_TOOBIG); 		
				}

				if(preg_match('/^([a-zA-Z]+)$/', $first))
				{
						echo "First Name contains only letters<br>";
						return(ERROR_JOIN_TRUE);
				}
				else
				{
						echo "First Name does not contain only letters<br>";
						return(ERROR_JOIN_FIRSTNAME_NONLETTERS);
				}
		}

		function validate_lastname($last)
		{
				/* last name must no more than 30 characters
				   can only contain a-z and A-Z  characters	*/

				if(strlen($last)==LASTNAME_MAX_SIZE)
				{
						echo "last name size too big<br>";
						return(ERROR_JOIN_LASTNAME_TOOBIG); 		
				}

				if(preg_match('/^([a-zA-Z]+)$/', $last))
				{
						echo "Last Name contains only letters<br>";
						return(ERROR_JOIN_TRUE);
				}
				else
				{
						echo "Last Name does not contain only letters<br>";
						return(ERROR_JOIN_LASTNAME_NONLETTERS);
				}
		}

?>

