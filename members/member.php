<?php
		/*member.php*/
		require_once($_SERVER['DOCUMENT_ROOT']."/utils.php");


		if(isset($_SERVER['HTTP_REFERER']) )
				$referer	=	$_SERVER['HTTP_REFERER'];	
		else
		{
				if(checkuserlogin()==FALSE)
				{
						$referer	= '';
						$root_match	=	"/myproject.com\/login.php/";
						if (!preg_match("$root_match", $referer) )
						{
								header('Location: '.ROOT_SITE);
								exit;
						}
				}

		}

		
?>

<?php
		require_once($_SERVER['DOCUMENT_ROOT']."/members/header_members.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/members//member_content.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/footer.php");
?>
