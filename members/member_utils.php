<?php									     


		require_once($_SERVER['DOCUMENT_ROOT']."/db/db_tags.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/db/db_posts.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/db/db_options.php");

		global $RECENT_POSTS_BYDAY;

		function getrecenttags($conn)
	    {
						
			/* get unique tags from most recent post */

		    $RECENT_POSTS_BYDAY = 	db_getoptions_recent_post_byday($conn);

			$posts=db_getposts_byday($conn, $RECENT_POSTS_BYDAY);
			$numberofposts=count($posts);
			$Tags=array();
			$UniqueTags=array();
			$Tags_index=0;	
			for($i=0;$i<$numberofposts;$i++)
			{
					/* get a post */
					$post=$posts[$i]['id'];
					//echo "post ".$post."<br>";		
					/* get tags */
					$tags= db_gettags_bypost($conn, $post);
					/*check if no tags retruned for a post */
					if($tags==0)
						continue;
				

					$numberoftags=count($tags);

					for($j=0;$j<$numberoftags;$j++)
					{
							//echo "tag ".$j.$tags[$j]['tag_id']."<br>";
							$Tags[$Tags_index] = $tags[$j]['tag_id'];
							$Tags_index++;
					}
			}
													
													
			/* get the unique tag */
			$UniqueTags = array_unique($Tags);
			/* re index the UniqueTags array */	
			$UniqueTags = array_values($UniqueTags);
																			
			return($UniqueTags);
		}
										    
		function create_tagbuttons()
		{
			/* create buttons from most recent tags */

			$conn=db_connect();
			if(db_connect_ok($conn)==false)
			{
					printf("%s",db_connect_msg());
					printf("%d",db_connect_errorcode());
					exit();
			}


			$t = getrecenttags($conn);
			$numberoftags = count($t);		
												                                                          
			echo "<div align='center'>";
			for($i=0;$i<$numberoftags;$i++)
			{	
				$tag_text=db_gettagtext($conn, $t[$i]);
				echo "<button name='button' type='button' onClick=loadpost(".
			    json_encode($tag_text['text']).
     			 ",'postslist')>"
				.$tag_text['text']."</button>";
			}

			echo "</div>"; 

	 }
										
?>
