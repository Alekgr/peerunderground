<?php
				require_once($_SERVER['DOCUMENT_ROOT']."/db/db_functions.php");
				require_once($_SERVER['DOCUMENT_ROOT']."/db/db_posts.php");
				require_once($_SERVER['DOCUMENT_ROOT']."/db/db_users.php");
				require_once($_SERVER['DOCUMENT_ROOT']."/utils.php");



				$conn = 	db_connect();												
				if(db_connect_ok($conn)==false)
				{
					printf("%s ",db_connect_msg($conn));
					printf("%d ",db_connect_errorcode($conn));
					exit(0);
				}

				$post_p_id 	=	$_GET['p'];
				$post_id	=	db_getpost_id($conn, $post_p_id);
				$post_comments = db_getpostcomment($conn, $post_id['id']);							
			
				$numberofcomments = count($post_comments);	
				
				for($i=0;$i<$numberofcomments;++$i)
				{
						$comment = 	$post_comments[$i]['text'];
						$author_id  =	$post_comments[$i]['pc_author'];			
						$date		=	$post_comments[$i]['pc_date'];
						$author		=	db_getusername($conn,$author_id);		

						echo "<div>";
						echo "<div style='display:inline-block'>"."<img src=".getuserprofilesmalliconpath($author)." style:border:solid 1px green'>"."</img>". "</div>";
						echo "<div style=width:25%;margin-left:7px;margin-right:30px;font-size:small;display:inline-block;vertical-align:top;word-wrap:break-word>";
						echo "<span style='color:gray'>".$author."</span>";
						echo "<span style='color:blue;font-size:xx-small;padding-right:100px'>".get_date_translation($date)."</span>";
						echo "<div>".$comment."</div>";
						echo "</div>";
						echo "</div>";
				}


				function get_date_translation($date)
				{
						/* get date difference from now() */	
						$now = 		time();

						/* convert $date to unix time format */
						$date = 	strtotime($date);

						/* get time difference in seconds */
						$diff = 	$now-$date;	
						
						/*translate to appropriate time diff */
						$time_t=get_english_time_diff($diff);
												
						return $time_t;
						
				}	
				

						
				function get_english_time_diff($diff)
				{

						switch($diff)
						{
										case ($diff<60): 	$timezone="Seconds"; $t=getseconds($diff);break;
										case ($diff<(60*60)): $timezone="Minutes"; $t=getminutes($diff);break;
										case ($diff<(60*60*24)):	$timezone="Hour";$t=gethours($diff);break;
										case ($diff<(60*60*24*365)):	$timezone="Day";$t=getdays($diff);break;
										default: $timezone="Year"; $t=getyears($diff);break;
						}

						return $t. " ".$timezone." ago";
				}

				function getseconds($diff)
				{
						return $diff;
				}

				function getminutes($diff)
				{
						return floor( ($diff/(60)) );
				}	
				
				function gethours($diff)
				{
						return floor( ($diff/(60*60)) ) ;
				}

				function getdays($diff)
				{
						return  floor( ($diff/(60*60*24)) );
				}

				function getyears($diff)
				{
						return floor( ($diff/(60*60*24*365)) );
				}


