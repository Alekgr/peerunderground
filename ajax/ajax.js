//javascript ajax funcitons

const COMMENT_TIMEOUT=15000;

function loadpost(tag, box)
{
		//load posts that have a tag
		var xmlhttp	=	new XMLHttpRequest();
		xmlhttp.onreadystatechange =	function()
		{
				if(xmlhttp.readyState	==	4 && xmlhttp.status == 200)
				{
						document.getElementById(box).innerHTML	=	xmlhttp.responseText;
				}

		}
		xmlhttp.open("GET","/ajax/post.php?t="+tag,true);
		xmlhttp.send();

}

function loadpostcomments(post,box)
{

		var xmlhttp	=	new XMLHttpRequest();
		xmlhttp.onreadystatechange =	function()
		{
				if(xmlhttp.readyState 	==	4 && xmlhttp.status == 200)
				{
						document.getElementById(box).innerHTML =	xmlhttp.responseText;
				}

		}
		xmlhttp.open("GET","/ajax/comment.php?p="+post,true);
		xmlhttp.send();
	
		setTimeout(function() { loadpostcomments(post,box) },COMMENT_TIMEOUT);
	}

function insertcomment(post)
{
		
		//get the comment text from textarea
		var comment =	document.getElementsByName('user_comment')[0].value;
		var xmlhttp	=	new XMLHttpRequest();

		//clear the textarea
		document.getElementsByName('user_comment')[0].value =	"";	

		xmlhttp.onreadystatechange	=	function()
		{
				if(xmlhttp.readyState	==	4 && xmlhttp.status	==	200)
				{
						document.getElementById('comment_msg').innerHTML	=	xmlhttp.responseText;
				}
		}

		//post request with post and comment
		xmlhttp.open("POST","/ajax/submitcomment.php",true);
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xmlhttp.send("p="+post+"&"+"c="+comment);
}


