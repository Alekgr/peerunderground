<?php

		require_once($_SERVER['DOCUMENT_ROOT']."/db/db_functions.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/db/db_tags.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/db/db_posts.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/config.php");

		$tag = $_GET["t"];

		/* connect to db */
		$conn = db_connect();
		if(db_connect_ok($conn)==false)
		{
				printf("%s ",db_connect_msg($conn));
				printf("%d ",db_connect_errorcode($conn));
				exit(0);
		}

		/* get tag's id */
		$id_result = db_gettagid($conn, $tag);

		/* if return 0 something went wrong */
		if($id_result==0)
				return;

		/* assign tag id */
		$tag_id = $id_result['id'];
	
		/* display posts */
		$posts_result=db_getposts_bytag($conn,$tag_id);
		$numberofposts = count($posts_result);		
		for($i=0;$i<$numberofposts;++$i)
		{
				$post	=	$posts_result[$i]['post_id'];
				$post_pid	=	db_getpostp_id($conn,$post);
			    $post_title = db_getposttitle($conn, $post);
				$url=constant("POST_SITE")."?p=".$post_pid['p_id'];
				echo "<a href=",$url,">",$post_title['p_title'],"</a>";
				echo "<br>";
		}
		

?>
