<?php

		require_once($_SERVER['DOCUMENT_ROOT']."/db/db_functions.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/defs.php");


		function db_gettags_bypost($conn, $postid)
		{
				/* get tags fro a post */
				
				$query = "select tag_id from tag_xref_post where post_id=$postid";
				$result	=	db_query($conn,$query);
				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result, $query);
			
				if($result->num_rows==0)
						return(0);						

				while($rows = $result->fetch_array(MYSQL_ASSOC))
				{
						$user_data[] = $rows;
				}

				return $user_data;
		}

		function db_gettagtext($conn, $tagid)
		{
				/* get a tag's text */

				$query = "select text from tag where id=$tagid limit 1";
				$result	=	db_query($conn, $query);
				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result,$query);

				if($result->num_rows==0)
						return(0);

									
				$user_data = $result->fetch_array(MYSQL_ASSOC);

				return $user_data;
		}

		function db_gettagid($conn, $tag_text)
		{
				/* get a tag's id */
				
				$query = "select id from tag where text='$tag_text'";
				$result =	db_query($conn, $query);

				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result,$query);
				
				if($result->num_rows==0)
						return(0);

				$user_data = $result->fetch_array(MYSQL_ASSOC);
				return $user_data;
		}
?>
