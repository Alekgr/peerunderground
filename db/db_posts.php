<?php

        require_once($_SERVER['DOCUMENT_ROOT']."/db/db_functions.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/defs.php");


		function db_insertpost($conn,$post_title,$post_author,$post_date,$post_data, $p_id)		
		{
				/* insert a post into a database */

				$query = "INSERT INTO post (p_title,p_author,p_date,p_data,p_id) VALUES('$post_title','$post_author','$post_date','$post_data','$p_id')";								
				$result = db_query($conn, $query);

				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result, $query);

				return($result);	
		}

		function db_insertcomment($conn,$p_id, $post_author,$text)
		{
				/* insert a comment into the database */

				$query	=	"INSERT INTO post_comment (text,pc_post,pc_author,pc_date) VALUES('$text',$p_id,'$post_author',now())";
				$result =	db_query($conn, $query);

				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result,$query);

				return($result);
		}

		function db_getpostauthor($conn, $post_id)
		{

				/* return post author */	
				
				$query = "SELECT p_author from post where id=$post_id limit 1";
				$result =	db_query($conn,$query);

				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result, $query);

				$user_data =	$result->fetch_array(MYSQL_ASSOC);
				return $user_data;
		}

		function db_getauthorsposts($conn, $author)
		{
				/* get author's posts */
				
				$query = 	"SELECT id from post where p_author=$author";
				$result=	db_query($conn,$query);
				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result,$query);

				while($rows = $result->fetch_array(MYSQL_ASSOC))
				{
						$user_data[] = $rows;
				}

				return $user_data;

		}

		function db_getposts_bytag($conn, $tagid)
		{
				/* get posts from a tag */
				$query = "select post_id from tag_xref_post where tag_id=$tagid";
				$result	=	db_query($conn,$query);
				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result, $query);
				
				while($rows = $result->fetch_array(MYSQL_ASSOC))
				{
						$user_data[] = $rows;
				}

				return $user_data;
		}

		
		function db_getposts_byday($conn, $day)
		{
				$query =	"select id from post where TIMESTAMPDIFF(day,post.p_date,now()) < $day";
				$result = db_query($conn,$query);
				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result,$query);

				while($rows = $result->fetch_array(MYSQL_ASSOC))
				{
						$user_data[] = $rows;
				}

				return $user_data;
		}

		function db_getposttitle($conn, $post)
		{
				/* get a post's title */
				
				$query	=	"select p_title from post where id=$post limit 1";
				$result	=	db_query($conn,$query);	

				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result,$query);

				$user_data = $result->fetch_array(MYSQL_ASSOC);
		
				return($user_data);

		}

		function db_getpostdata($conn, $post_id)
		{
				/* get a post's data */

				$query 	=	"select p_data from post where p_id='$post_id' limit 1";
				$result	=	db_query($conn,$query);

				if(defined("DEBUG"))
						db_query($conn,$query);

				$user_data = $result->fetch_array(MYSQL_ASSOC);
				return($user_data);
		}

		function db_getpostall($conn, $post_id)
		{
				/* get post's data all all columns*/

				$query 	=	"select p_title,p_author,p_date,p_data from post where p_id='$post_id' limit 1";
				$result	=	db_query($conn,$query);

				if(defined("DEBUG"))
						db_query($conn,$query);

				$user_data = $result->fetch_array(MYSQL_ASSOC);
				return($user_data);
		}



		function db_getpostp_id($conn, $post)
		{
				/* get post's p_id */

				$query	=	"select p_id from post where id=$post limit 1";
				$result =	db_query($conn,$query);
				
				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result,$query);
				
				$user_data	=	$result->fetch_array(MYSQL_ASSOC);
				return($user_data);
		}

		function db_getpost_id($conn, $p_id)
		{

				/* get posts p_id by id */

				$query  = 	"select id from post where p_id='$p_id' limit 1";
				$result =	db_query($conn,$query);

				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result,$query);

				$user_data	= $result->fetch_array(MYSQL_ASSOC);
				return($user_data);
		}

		function db_getpostcomment($conn, $id)
		{
				/* get post's comment by post id */
				
				$query = 	"select text,pc_author,pc_date from post_comment where pc_post=$id";
							
				$result = 	db_query($conn,$query);

				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result,$query);


				while($rows = $result->fetch_array(MYSQL_ASSOC))
				{
						$user_data[]	=	$rows;				
				}

				return($user_data);
		}

		function db_checkposturlidforuniqness($conn, $url_id)
		{
				/* check url_id for uniqness */
				/* if it is not unique return false */

				
				$query	=	"select p_id from post where p_id='$url_id' limit 1";
				$result =	db_query($conn, $query);
				
				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result,$query);

				if($result->num_rows>0)
						return false;	
				else
						return true;
		}
?>
