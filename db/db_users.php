<?php
		require_once($_SERVER['DOCUMENT_ROOT']."/db/db_functions.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/defs.php");	

		/** db functions to retriving users **/

		function db_getuserid($conn, $username)
		{

				/*get user id of the username*/

				$query	=	"select id from users where username='$username'";
				$result	=	db_query($conn, $query);		
				
				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result, $query);	
				
				
				$user_data	=	$result->fetch_array(MYSQL_ASSOC);
				return($user_data['id']);
				
		}


		function db_getusername($conn, $id)
		{
				/*get username from an id*/

				$query	=	"select username from users where id=$id";
				$result	=	db_query($conn, $query);

				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result, $query);

				$user_data	=	$result->fetch_array(MYSQL_ASSOC);
				return($user_data['username']);

		}


		function db_get_firstlastemail($conn, $username)
		{
				/*get user's first,last,email for the $username*/
		
				$query	=	"select first,last,email from users where username='$username'";	
				$result	=	db_query($conn,$query);	
				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result,$query);

				$user_data	=	$result->fetch_array(MYSQL_ASSOC);
				return $user_data;
		}

		function db_getuseremail($conn, $username)
		{
				/*get username email*/
				
				$query	=	"select email form users where username='$username'";
				$result	=	db_query($conn,$query);
				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result,$query);

				$user_data	=	$result->fetch_array(MYSQL_ASSOC);
				$email	=	$user_data['email'];
				return($email);

		}

		function db_getuserhash($conn, $username)
		{
				/*get username hash*/
				
				$query	=	"select hash from users where username='$username'";
				$result	=	db_query($conn,$query);
				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result,$query);

				$user_data	=	$result->fetch_array(MYSQL_ASSOC);
				$hash	=	$user_data['hash'];
				
				return $hash;

		}


		function db_getuserpermission($conn,$username)
		{
				/*get permissions form username*/

				$query	=	"select p_user,adminflg from permissions where p_user IN ( select id from users where username = '$username')";
				$result	=	db_query($conn,$query);
				if(defined("DEBUG"))
						db_query_DEBUG($conn,$result,$query);


				$user_data	=	$result->fetch_array(MYSQL_ASSOC);
				$permission	=	$user_data['adminflg'];

				return $permission;
		}

			
		function db_insertnewuser($conn, $first,$last,$username,$hash,$email)
		{
				/*insert a new user into a database*/

				$query="insert INTO users (username,hash,first,last,email) VALUES('$username', '$hash','$first','$last','$email')";

				$result	=	db_query($conn, $query);
				
				if(defined("DEBUG"))
					db_query_DEBUG($conn, $result,$query);

				return($result);	

		}

?>		
