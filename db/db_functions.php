<?php

		/** db functions  **/

		require_once($_SERVER['DOCUMENT_ROOT']."/defs.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/db/db_errors.php");

		function db_connect_msg($conn)
		{
				/*return connect error*/
				return($conn->connect_error);
		}

		function db_connect_errorcode($conn)
		{
				/*return connect error number*/
				return($conn->connect_errno);
		}

		function db_connect_ok($conn)
		{
				/*return true if no errors*/
				if($conn->connect_error)
						return false;
				else
						return true;
		}

		function db_query_msg($conn)
		{
				/*return query error*/
				return($conn->error);
		}
		
		function db_query_errorcode($conn)
		{
				/*return query errorno*/
				return($conn->errno);
		}


		function db_query_ok($result)
		{
				/*return true if query is ok*/
				if($result==true)
						return(true);
				if($result==false)
						return(false);
				else
						return(true);
		}



		function db_connect()
		{
				/*connect to db */
				/*return mysqli object*/

				$conn 	=	new mysqli($_SERVER['DB_HOST'],$_SERVER['DB_USER'],$_SERVER['DB_PASS'],$_SERVER['DB_NAME'],$_SERVER['DB_PORT']);
				
				return $conn;
		}

		
		function db_close($conn)
		{
				/*close db connection */
				$result=$conn->close();
				return $result;
		}


		function db_query($conn, $query)
		{
				/*do a query*/

				/*will return $mysqli_result if query is select,show,explain, describe* or false on failure/
				/*for other succesful queries it will return true and false for error*/
				
				/*make sure this is clean query*/

				$result =	$conn->query($query);
				return($result);
		}

		function db_query_DEBUG($conn, $result, $query)
		{
				
				/*write debug info to file*/
		
				openlog("DB",LOG_PID|LOG_PERROR,LOG_LOCAL0);
				
				$time	=	date("Y/m/d H:i:s");
				
				if(db_query_ok($result)==false)			
				{
						$msg=db_query_msg($conn);
						$errcode=db_query_errorcode($conn);
						syslog(LOG_ERROR,"[$time]$msg $errcode", db_query_msg($conn));
				}
				else
				{
						syslog(LOG_INFO,"Query:$query was successfull Rows:$conn->affected_rows");
				}

				closelog();
		}
		

?>
