<?php
		/*postings.php*/
		require_once($_SERVER['DOCUMENT_ROOT']."/utils.php");

		if(isset($_SERVER['HTTP_REFERER']) )
				$referer	=	$_SERVER['HTTP_REFERER'];	
		else
		{
				if(checkuserlogin()==FALSE)
				{
						$referer	= '';
						$root_match	=	"/myproject.com\/login.php/";
						if (!preg_match("$root_match", $referer) )
						{
								header('Location: '.ROOT_SITE);
								exit;
						}
				}

		}

		
?>

<?php 	
		require_once($_SERVER['DOCUMENT_ROOT']."/header_postings.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/postings_content.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/footer.php"); 
?>	
