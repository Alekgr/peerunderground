<?php
		/*admin.php*/
		require_once($_SERVER['DOCUMENT_ROOT']."/utils.php");
		if(isset($_SERVER['HTTP_REFERER']) )
				$referer	=	$_SERVER['HTTP_REFERER'];	
		else
		{
				if(checkuserlogin()==FALSE)
				{
						$referer	= '';
						$root_match	=	"/myproject.com\/login.php/";
						if (!preg_match("$root_match", $referer) )
						{
								header('Location: '.ROOT_SITE);
								exit;
						}

				}
				else
				{
						if(checkpermission($_SESSION['username'])!=ADMIN)
								header('Location: '.MEMBERS_SITE);
									

				}

		}

		
?>

				
		
<?php 
		require_once($_SERVER['DOCUMENT_ROOT']."/admin/header_admin.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/admin/admin_content.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/footer.php");
?>
