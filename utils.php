<?php

		/* some small utils functions */		

		require_once($_SERVER['DOCUMENT_ROOT']."/config.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/db/db_users.php");

		/* icons constants */
		define("BIG_PROFILE_ICON_SIZE", "128x128");
		define("SMALL_PROFILE_ICON_SIZE", "28x28"); 	
		define("PROFILE_ICON_FILETYPE", ".png");
		define("PROFILE_ICON_ROOT_PATH", "/profile");

		function removeusersession()
		{

		/* destroys all traces of session data */
	
				session_start();
				
				setcookie("username","", 1,"/"); /*remove cookie from browser*/
				$_SESSION=array(); /*unset _SESSION variable*/
				if(session_destroy()) /*destroy session from memory*/
				{
						header('Location: '.ROOT_SITE);
						exit;
				}
		}

		function currentuser()
		{
				session_start();
				return($_SESSION['username']);

		}

		function checkuserlogin()
		{
				/* check to a user is loged in */
				
				session_start();
				if(isset($_SESSION['username']))
						return TRUE;
				else
						return FALSE;
		}

		function checkpermission($username)
		{
				/*connect to mysql and check if user is admin*/
				
				$conn	=	db_connect();
				if(db_connect_ok($conn)==false)
				{
						printf("%s",db_connect_msg($conn));			
						printf("%s",db_cconect_errorcode($conn));
				}
						
				$permission =	db_getuserpermission($conn,$username);

				return $permission;
		}

		
		function isadmin($username)
		{
				if(checkpermission($username)==1)
						echo "admin";
				else
						echo "user";
		}

				
		function makeposturlid()
		{

				/* make post id */
				/* get a random unique sha1 40 chars id */
				/* browse the database to make sure there is another id */
		
				$conn = db_connect();
				if(db_connect_ok($conn)==false)
				{
						printf("%s ",db_connect_msg($conn));
						printf("%d ",db_connect_errorcode($conn));
						exit(0);
				}

				do{
						$p_id = sha1( uniqid(mt_rand(),true));
						$is_unique=db_checkposturlidforuniqness($conn,$p_id);
						if($is_unique==false) 
								echo "false";
				}
				while($is_unique==false);


				db_close($conn);		
		
				return($p_id);
		}

		function getuserprofilebigiconpath($user)
		{
				return PROFILE_ICON_ROOT_PATH."/".$user.BIG_PROFILE_ICON_SIZE.PROFILE_ICON_FILETYPE;
		}


		function getuserprofilesmalliconpath($user)
		{
				return PROFILE_ICON_ROOT_PATH."/".$user.SMALL_PROFILE_ICON_SIZE.PROFILE_ICON_FILETYPE;

		}

?>


