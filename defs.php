<?php

		/* 	defins some constants used for our application */

		/* contants for permissions */
		define("USER", 2);
		define("ADMIN",1);

		/*	constants for form sizes */
		define("FIRSTNAME_MAX_SIZE",	20);
		define("LASTNAME_MAX_SIZE",		30);				
		define("PASSWORD_MAXSIZE",		80);
		define("USERNAME_MAXSIZE",		30);
		define("EMAIL_MAXSIZE",			90);

		/*	error message	*/
		define("ERROR_JOIN_TRUE",	0);
		define("ERROR_JOIN_ERROR",	100);
		define("ERROR_JOIN_FIRSTNAME_TOOBIG",	101);
		define("ERROR_JOIN_FIRSTNAME_EMPTY",	102);
		define("ERROR_JOIN_FIRSTNAME_NONLETTERS",	103);
		define("ERROR_JOIN_LASTNAME_TOOBIG",	104);
		define("ERROR_JOIN_LASTNAME_EMPTY",	105);
		define("ERROR_JOIN_LASTTNAME_NONLETTERS",	106);
		define("ERROR_JOIN_PASSWORD_TOOBIG",	107);
		define("ERROR_JOIN_PASSWORD_EMPTY",	108);
?>
