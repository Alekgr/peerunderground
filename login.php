<?php 

		/*login.php:	This php will verify the user password */


		require($_SERVER['DOCUMENT_ROOT']."/config.php");
		require($_SERVER['DOCUMENT_ROOT']."/db/db_functions.php");
		require($_SERVER['DOCUMENT_ROOT']."/db/db_users.php");
		
		session_start();

		/*reporting errors*/
		error_reporting(E_ALL);

		/*send the client back to main page if trying load login.php directly*/
		if(isset($_SERVER['HTTP_REFERER']) )
				$referer	=	$_SERVER['HTTP_REFERER'];	
		else
		{
				$referer	= '';
				/*$root_match	=	"/myproject.com/";*/
				if (!preg_match(ROOT_SITE_MATCH, $referer) )
				{
						header('Location: '.ROOT_SITE);
						exit;
				}
		}

		/* get the posts*/
		$username_submited	=	$_POST['username']; 	
		$password_submited	=	$_POST['password'];		

		
		/*connect to mysql and get the hash*/
		
		$conn	=	db_connect();
		if(db_connect_ok($conn)==false)
		{
				printf("%s", db_connect_msg($conn));
				printf("%d", db_connect_errorcode($conn));
		}

		$hash	=	db_getuserhash($conn, $username_submited);	
		

		/* check password submited is == to the hash in the db */
		if(crypt($password_submited,$hash)==$hash)
		{
				$_SESSION['username'] = $username_submited;
				header('Location: ' .MEMBERS_SITE);
		}
		else
				echo "Wrong account or password<br>";
		
		?>

