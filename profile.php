<?php
		/*profile.php*/

		require_once($_SERVER['DOCUMENT_ROOT']."/utils.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/config.php");		

		if(isset($_SERVER['HTTP_REFERER']) )
				$referer	=	$_SERVER['HTTP_REFERER'];	
		else
		{
				if(checkuserlogin()==FALSE)
				{
						$referer	= '';
						$root_match		=	ROOT_MATCH_SITE_LOGIN;
						if (!preg_match("$root_match", $referer) )
						{
								header('Location: '.ROOT_SITE);
								exit;
						}
				}

		}

		
?>

<?php 
		require_once($_SERVER['DOCUMENT_ROOT']."/header_profile.php");  
		require_once($_SERVER['DOCUMENT_ROOT']."/profile_content.php"); 
	    require_once($_SERVER['DOCUMENT_ROOT']."/footer.php");
?>
